﻿using PerreraIQ.Models;
using PerreraIQ.Models.Persona;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PerreraIQ.Controllers.Persona
{
    public class PersonaAdoptController : Controller
    {
        protected readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: PersonaAdopt
        public ActionResult PersonaAdopt()
        {
            return View();
        }

        // GET: PersonaAdopt/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(PersonaAdopt personaAdopt)
        {
            if (ModelState.IsValid)
            {
                db.PersonaAdoptadas.Add(personaAdopt);
                db.SaveChangesAsync();
                return Content("success");
            }
            return View(personaAdopt);
        }
        // POST: PersonaAdopt/Create
        //[HttpPost]
        //public async Task<ActionResult> Create([Bind(Include = "PersonaAdoptId,Nombre,Descripcion,Direccion,Email,Telefono,FechaAdopcion")] PersonaAdopt personaAdopt)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.PersonaAdoptadas.Add(personaAdopt);
        //        await db.SaveChangesAsync();
        //        return Content("success");
        //    }

        //    return View(personaAdopt);
        //}

        // GET: PersonaAdopt/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PersonaAdopt/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PersonaAdopt/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PersonaAdopt/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}