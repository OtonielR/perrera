﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PerreraIQ.Helpers;
using PerreraIQ.Models.Animals;
using PerreraIQ.Models.Persona;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PerreraIQ.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //public ApplicationDbContext()
        //    : base("DefaultConnection", throwIfV1Schema: false)
        //{
        //}

        public ApplicationDbContext()
             : base(DbConnectionHelper.GetRDSConnectionString(), throwIfV1Schema: false)
        {
        }

        public DbSet<PersonaAdopt> PersonaAdoptadas { get; set; }
        public DbSet<PersonaDonar> PersonaDonadoras { get; set; }
        public DbSet<Raza> Razas { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<TipoAnimal> TipoAnimals { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}