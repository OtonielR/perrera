﻿using System.ComponentModel.DataAnnotations;

namespace PerreraIQ.Models.Animals
{
    public class Animal
    {
        public int AnimalId { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Display(Name = "Edad")]
        public int Edad { get; set; }

        public int TipoAnimalId { get; set; }

        [Display(Name = "Tipo de Animal")]
        public virtual TipoAnimal TipoAnimal { get; set; }

        public int RazaId { get; set; }

        [Display(Name = "Raza")]
        public virtual Raza Raza { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}