﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PerreraIQ.Models.Persona
{
    public class PersonaAdopt
    {
        public int PersonaAdoptId { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name ="Correo Electronico")]
        public string Email { get; set; }

        [Required]
        [StringLength(16)]
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }

        [Display(Name = "Fecha de Adopción")]
        public DateTime FechadeAdopcion { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}