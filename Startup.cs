﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PerreraIQ.Startup))]
namespace PerreraIQ
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
