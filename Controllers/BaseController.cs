﻿using PerreraIQ.Models;
using System.Linq;
using System.Web.Mvc;

namespace PerreraIQ.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Obtiene el usuario que hizo la solicitud
        /// </summary>
        /// <returns></returns>
        protected ApplicationUser GetRequestUser()
        {
            return db.Users.SingleOrDefault(x => x.UserName == User.Identity.Name);
        }

        /// <summary>
        /// Sobreescribimos el serializador por defecto para utilizar el de Json.Net
        /// </summary>
        /// <param name="data"></param>
        /// <param name="contentType"></param>
        /// <param name="contentEncoding"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        /// 

        //protected override JsonResult Json(object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
        //{
        //    return new JsonNetResult
        //    {
        //        Data = data,
        //        ContentType = contentType,
        //        ContentEncoding = contentEncoding,
        //        JsonRequestBehavior = behavior
        //    };
        //}

        //protected override void Initialize(RequestContext requestContext)
        //{
        //    base.Initialize(requestContext);

        //    if (WebProvider.ActiveProvider != null)
        //    {
        //        // Perform device detection on the headers provided in the
        //        // request.
        //        var match = WebProvider.ActiveProvider.Match(
        //            requestContext.HttpContext.Request.Headers);

        //        // Create a model that is based on the match request from
        //        // device detection.
        //        var device = new Device(match);
        //        ViewBag.Device = device;

        //        // Also expose the match result directly in the ViewBag
        //        // to compare the different access methods when used in the
        //        // view.i
        //        ViewBag.Match = match;

        //        EventLog.Info(device.PlatformName);
        //    }

        //    // Get the HTTP headers from the request to display their values in
        //    // the view.
        //    ViewBag.RequestHeaders =
        //        requestContext.HttpContext.Request.Headers;
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}