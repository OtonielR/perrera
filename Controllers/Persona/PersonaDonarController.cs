﻿using PerreraIQ.Models;
using PerreraIQ.Models.Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PerreraIQ.Controllers.Persona
{
    public class PersonaDonarController : Controller
    {
        protected readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: PersonaDonar
        public ActionResult PersonaDonar()
        {
            return View();
        }

        // GET: PersonaDonar/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(PersonaDonar personaDonar)
        {
            if (ModelState.IsValid)
            {
                db.PersonaDonadoras.Add(personaDonar);
                db.SaveChangesAsync();
                return Content("Succes");
            }
            return View(personaDonar);
        }

        // POST: PersonaDonar/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: PersonaDonar/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PersonaDonar/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PersonaDonar/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PersonaDonar/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
