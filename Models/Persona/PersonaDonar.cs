﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PerreraIQ.Models.Persona
{
    public class PersonaDonar
    {
        public int PersonaDonarId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name ="Descripción")]
        public string Descripcion { get; set; }

        [Required]
        [Display(Name ="Fecha de entrega")]
        public DateTime FechaEntrega { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}