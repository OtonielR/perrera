﻿using System.ComponentModel.DataAnnotations;

namespace PerreraIQ.Models.Animals
{
    public class Raza
    {
        public int RazaId { get; set; }

        [Required]
        [StringLength(255)]
        public string Nombre { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}