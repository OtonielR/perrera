﻿using System.ComponentModel.DataAnnotations;

namespace PerreraIQ.Models.Animals
{
    public class TipoAnimal
    {
        public int TipoAnimalId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name ="Nombre")]
        public string Nombre { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}